.PHONY: build watch clean
default: build

INSTALL_DIR=/usr/lib/node_modules/typescript/bin/
TSC=$(INSTALL_DIR)tsc
	
build:
	$(TSC)

watch:
	$(TSC) --watch

clean:
	rm -rf ./web/static/js
	mkdir ./web/static/js
	touch ./web/static/js/.empty
